package BeerStore;

public class BrowarMiloslawImpl implements BrowarMiloslaw, Browar {

    private final String BROWAR_NAME = "Miłosław";

    @Override
    public OrderBeerFromBrowarResponse orderBeer(
            OrderBeerFromBrowarRequest orderBeerFromBrowarRequest) {
        orderBeer(orderBeerFromBrowarRequest.amount, orderBeerFromBrowarRequest.beerType.beer());

        return null;
    }

    @Override
    public String getBrowarName() {
        return BROWAR_NAME;
    }

    @Override
    public void orderBeer(Integer amount, String beerName) {

    }
}
