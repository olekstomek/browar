package BeerStore;

import java.util.List;

public interface BeerStore {

    List<Beer> getAllAvailableBeers();

    List<Beer> sellBeer(BuyBeerRequest buyBeerRequest);

    void acceptBeer(OrderBeerFromBrowarRequest orderBeerFromBrowarRequest);

    BeerReport generateReport();

}