package BeerStore;

public class BrowarCzarnkowImpl implements BrowarCzarnkow, Browar {

    private final String BROWAR_NAME = "CZARNKOW";

    @Override
    public OrderBeerFromBrowarResponse orderBeer(
            OrderBeerFromBrowarRequest orderBeerFromBrowarRequest) {
        zamowPiwo(orderBeerFromBrowarRequest.beerType.beer(), orderBeerFromBrowarRequest.amount);

        return null;
    }

    @Override
    public String getBrowarName() {
        return BROWAR_NAME;
    }

    @Override
    public void zamowPiwo(String nazwa, Integer ilosc) {

    }
}
