package BeerStore;

public interface Browar {

    OrderBeerFromBrowarResponse orderBeer(OrderBeerFromBrowarRequest orderBeerFromBrowarRequest);

    String getBrowarName();

}