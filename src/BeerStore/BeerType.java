package BeerStore;

import java.util.EnumSet;

public enum BeerType {
    NOTECKIE_JASNE("Noteckie jasne"),
    NOTECKIE_CIEMNE_EIRE("Noteckie_ciemne_eire"),
    KOMES_PORTIER("Komes portier"),
    MILOSLAW_PILS("Miłosław pils");

    public static EnumSet<BeerType> browarCzarnkow = EnumSet.of(NOTECKIE_CIEMNE_EIRE, NOTECKIE_JASNE);
    public static EnumSet<BeerType> browarMiloslaw = EnumSet.of(KOMES_PORTIER, MILOSLAW_PILS);

    private String beer;

    BeerType(String beer) {
        this.beer = beer;
    }

    public String beer() {
        return beer;
    }
}
